# PacketStream

Residential Proxies Powered By Peer-To-Peer Bandwidth Sharing.

## How to run it?

- Clone the repository
- Run

```bash
docker-compose up -d
```

- To run multi packetstream client

```bash
docker-compose up -d --scale packetstream=60
```
